#!/usr/bin/env bash

# build image if it's the first run
if [[ "$(docker images -q gat-dbt 2> /dev/null)" == "" ]]; then
	docker build docker/dbt -t gat-dbt
fi

docker run -i --rm \
  -v $(pwd):/app \
  --network  bi-devtask_default \
  -w "/app" \
  gat-dbt \
  dbt-metabase models \
    --dbt_manifest_path ./target/manifest.json \
    --dbt_database analytics \
    --metabase_host metabase:3000 \
    --metabase_http \
    --metabase_user devtask@example.com \
    --metabase_password devtask1! \
    --metabase_database 'Warehouse' 
