#!/usr/bin/env bash

# build image if it's the first run
if [[ "$(docker images -q gat-dbt 2> /dev/null)" == "" ]]; then
	docker build docker/dbt -t gat-dbt
fi

docker run -it --rm \
  -v $(pwd):/app \
  -e "REDSHIFT_USER=root" \
  -e "REDSHIFT_PASSWORD=''" \
  --network  bi-devtask_default \
  -w "/app" \
  gat-dbt \
  bash
