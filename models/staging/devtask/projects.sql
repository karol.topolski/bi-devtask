{{
     config(
       materialized = 'table',
       unique_key = 'projectid'
     )

}}

SELECT
    id AS projectid
    , name AS projectname
    , description AS projectdescription
FROM devtask.projects
