# GAT BI team technical assessment

## Introduction

Welcome to the BI team's technical assessment! You're going to get in touch with the tools the BI team use on the daily basis to deliver meaningful data to our company:

- docker & docker-compose
- Amazon Redshift warehouse(well, [close open-source copy of it](https://github.com/HearthSim/docker-pgredshift) 😅)
- [DBT](https://www.getdbt.com) as an ETL pipeline
- [Metabase](https://www.metabase.com) for visualisations

## Installation and usage

Whole setup is dockerized, so you won't need to install much on your local environment.

### Dependencies

You're only going to need two tools installed on your local environment: `docker` and `docker-compose`.

1. [Installing docker](https://docs.docker.com/engine/install/)
1. [Installing docker-compose](https://docs.docker.com/compose/install/)

### Usage

In order to get started you need to execute three commands:

1. `docker-compose up`: this will boot all of the services specified in `docker-compose.yml` including:
    - Redshift warehouse
    - Postgres DB for storing internal metabase data
    - Metabase server
    - Time entries API(more on this later)
1. `bin/run-dbt.sh`: this will ensure that the ETL pipeline is fully operational and you're good to go with your technical assessment.
    - Wait with executing this command for all services to boot up(usually takes around a minute or two)
1. `bin/metabase-sync-metadata.sh`: this will ensure that metadata sync between DBT and metabase is operational.

## Technical assessment

### About the assessment

This assessment is not about solving a problem. It's about execution and checking what's your way of thinking and coding/collaboration habits. If you'd like to use any tools not specified in docker-compose, you're free to do so :).

### General instructions

1. Fork this repository
1. All of your changes should be made in a **private** fork of this repository.
1. All of your changes should be made on a branch that is separate from `main`.
1. Once your satisfied with your solution, please:
    - Create a Pull Request in your repository from the branch with changes to **your** `main` branch
    - Invite `@karol.topolski` user as a repository collaborator with `Read` role, so our reviewers can check out the code.

### Structure

This is the initial structure of this repository:

- `bin`: contains scripts which will help you accomplish your goal
- `docker`: contains Dockerfiles used to build this project
- `models`: contains directory structure for simple ETL pipeline using DBT
  - `projects.sql`: simple transformation on the `projects` table. It's primary purpose is to make sure that DBT pipeline indeed works during the setup
  - `devtask.yml`: contains metadata about the projects
- `dbt_project.yml`: setup for the DBT tool
- `.sqlfluff` & `.sqlfluffignore`: contains configuration for [SQLFluff linter](https://www.sqlfluff.com) which you can use by executing `bin/lint.sh`.

### Your role

You're going to deal with two data sources:

1. The CSV file which contains data about users(`users.csv`)
1. [Time entries API](http://localhost:3300/api/time_entries) which contains data from time logs each user fills when working on a given project(projects data is already loaded to your Metabase)

Your goal is to create a BI report(using metabase) with summary of time spent by an individual employee on projects the employee was involved in. Please make sure to **document** the actions/code you decide to take to accomplish the goal.

1. You need to find a way to get data from both data sources into the warehouse.
    - Please save data from Time entries API with `time_interval` as a JSONB field in the warehouse.
1. Once data is in the warehouse you will need to transform it into usable format.
    - Please keep in mind that the data which you will use for the report should be reusable for other kind of reports too.
    - Bonus style points for documenting the metadata on the way 😉
1. As the final step, you will need to create a report inside the Metabase.
    - Metabase can be reached in your browser on [localhost:3000](http://localhost:3000)
    - You should be already logged in, but in case you aren't the credentials are:
      - username: `devtask@example.com`
      - password: `devtask1!`
1. Let's say that the time entries data can change every few minutes. What would you do to ensure that your report is up to date?

### Troubleshooting

If you experience troubles with hanging metabase container, the best way to proceed is:

1. Kill all containers using `docker-compose stop`.
1. Restart them with `docker-compose start`.

If above doesn't fix the issue you might want to perform a full restart using `bin/full-restart.sh`. Please be aware that this will **wipe your data in both the warehouse and the metabase**.

If the issue persists please don't be shy to [contact me](mailto:karol.topolski@globalapptesting.com) :).
