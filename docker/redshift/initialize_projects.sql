CREATE SCHEMA devtask;

CREATE TABLE devtask.projects (
  id INTEGER,
  name VARCHAR(256),
  description VARCHAR(256)
);

-- Generated with Faker::Company from the ruby world :)
INSERT INTO devtask.projects VALUES
  (1, 'Crona Group', 'Switchable full-range project'),
  (2, 'OReilly, Predovic and Beier', 'Intuitive non-volatile infrastructure'),
  (3, 'Connelly-Hansen', 'Future-proofed 4th generation artificial intelligence'),
  (4, 'Turner, Cummings and Schuppe', 'Programmable background concept'),
  (5, 'Hirthe, Terry and Deckow', 'Re-contextualized fault-tolerant moderator');
