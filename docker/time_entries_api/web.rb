require "sinatra"
require "json"

get "/api/time_entries" do
  content_type :json

  File
    .read("./time_entries.json")
    .then(&JSON.method(:parse))
    .to_json
end
